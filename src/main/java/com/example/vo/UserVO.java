package com.example.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author 王琪云
 * @Description TODO
 * @Date 2023/6/1 20:58
 * @Version 1.0
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVO {
    private Long userId;
    private String userName;
    private String userPassword;
    private String userEmail;
    private Integer type;
    private Integer userAge;
    private Date createTime;
}
