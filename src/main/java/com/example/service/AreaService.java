package com.example.service;

import com.example.entity.PriceData;

import java.io.IOException;
import java.util.List;

/**
 * @author xqz
 * @description TODO
 * @dateTime 2023/5/30 12:23
 */
public interface AreaService {

    /**
     * 获取某年所有省份的价格数据
     * @author xqz
     * @apiNote 获取HBase中的数据,该数据是某一年所有省份的所有价格数据
     * @dateTime 2023/5/17 10:06
     * @param rowKey 行键
     * @return List<PriceData> 价格数据对象列表
     */
    List<PriceData> getAllYearOfProvinceData(String rowKey) throws IOException;

    PriceData getHBaseData(String province, String year, String month) throws IOException;

    int getOverallCount() throws IOException;

    int getProvinceCount() throws IOException;

    int getMarketCount() throws IOException;
}
