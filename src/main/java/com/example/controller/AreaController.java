package com.example.controller;

import com.example.entity.PriceData;
import com.example.service.AreaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

import static com.example.util.TimeConstant.MONTHS_COUNT;

/**
 * @author xqz
 * @description TODO
 * @dateTime 2023/5/30 12:21
 */
@Slf4j
@Controller
@RequestMapping("/area")
public class AreaController {

    @Resource
    private AreaService areaService;

    @GetMapping
    public String areaIndex() {
        return "area_price";
    }

    /**
     * 获取某一年所有省份的所有平均价格
     * @author xqz
     * @apiNote 获取某一年所有省份的所有平均价格
     * @dateTime 2023/5/21 9:45
     * @param year 某年(2018-2021)
     */
    @GetMapping("/data/year")
    @ResponseBody
    public List<PriceData> areaPriceByYear(@RequestParam String year) {
        List<PriceData> priceDataList = new ArrayList<>();
        try {
            priceDataList = areaService.getAllYearOfProvinceData(year);
            // 获取最大值和最小值,第一个元素为最小值 第二个元素为最大值
            PriceData minValueData = priceDataList.get(0);
            PriceData maxValueData = priceDataList.get(1);

            // 从列表中删除最大值和最小值对象
            priceDataList.remove(minValueData);
            priceDataList.remove(maxValueData);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return priceDataList;
    }

    @GetMapping("/page/welcome")
    public String welcome(Model model) throws IOException {
        int dataTotalCount = areaService.getOverallCount();
        int marketTotalCount = areaService.getMarketCount();
        int provinceTotalCount = areaService.getProvinceCount();
        model.addAttribute("dataTotalCount", dataTotalCount);
        model.addAttribute("marketTotalCount", marketTotalCount);
        model.addAttribute("provinceTotalCount", provinceTotalCount);
        return "welcome";
    }

    @GetMapping("/page/province")
    public String toAreaPageByProvince() {
        return "area_different_year";
    }

    @GetMapping("/page/year/change")
    public String toAreaPageByYearChange() {
        return "area_year_change";
    }

    @GetMapping("/page/province/month/change")
    public String toAreaPageByProvinceAndYearAndMonthChange() {
        return "area_price_day";
    }

    /**
     * 展示1: 获取某省份所有年份所有月份的数据
     * @author xqz
     * @apiNote 获取某省份所有年份所有月份的数据
     * @dateTime 2023/5/17 20:56
     * @param province 省份
     */
    @GetMapping("/data/province")
    @ResponseBody
    public ResponseEntity<Map<String, List<PriceData>>> areaPriceByProvince(@RequestParam String province) {
        // 定义年份
        List<String> years = Arrays.asList("2018", "2019", "2020", "2021");
        // 初始化一个集合, 用来装查询到的数据
        Map<String, List<PriceData>> dataMap = new HashMap<>(48);
        try {
            for (String year : years) {
                List<PriceData> dataList = new ArrayList<>();
                for (int i = 1; i <= MONTHS_COUNT; i++) {
                    PriceData data = areaService.getHBaseData(province, year, String.format("%02d", i));
                    dataList.add(data);
                }
                dataMap.put(year, dataList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(dataMap, HttpStatus.OK);
    }

    /**
     * 返回数据总量
     * @author xqz
     * @apiNote 返回数据总量
     * @dateTime 2023/6/1 11:23
     * @return int
     */
    @GetMapping("/data/total")
    @ResponseBody
    public int getDataCount() {
        int dataCount = 0;
        try {
            dataCount = areaService.getOverallCount();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataCount;
    }

    /**
     * 返回市场数据总量
     * @author xqz
     * @apiNote 返回市场数据总量
     * @return int
     */
    @GetMapping("/data/market/total")
    @ResponseBody
    public int getDataMarketCount() {
        int dataCount = 0;
        try {
            dataCount = areaService.getMarketCount();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataCount;
    }

    /**
     * 返回省份数据总量
     * @author xqz
     * @apiNote 返回省份数据总量
     * @return int
     */
    @GetMapping("/data/province/total")
    @ResponseBody
    public int getDataProvinceCount() {
        int dataCount = 0;
        try {
            dataCount = areaService.getProvinceCount();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataCount;
    }
}
