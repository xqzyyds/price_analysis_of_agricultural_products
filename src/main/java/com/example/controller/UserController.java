package com.example.controller;

import com.example.entity.User;
import com.example.service.UserService;
import com.example.vo.DataVO;
import com.example.vo.UserVO;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Author 王琪云
 * @Description TODO
 * @Date 2023/6/1 20:21
 * @Version 1.0
 */

@Slf4j
@Controller
@RequestMapping("/user")
public class UserController {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public UserService userService;

    /**
     * 跳转到用户列表
     *
     * @return
     */
    @GetMapping("/page/userList")
    public String userList() {
        return "user_list";
    }

    /**
     * 查询所以的用户
     *
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/page/queryAllUser")
    @ResponseBody
    public DataVO<UserVO> queryAllUser(Integer page, Integer limit
            , @RequestParam(value = "userName", required = false) String userName) {

        DataVO<UserVO> dataVO = new DataVO<>();
        //为dataVO设置：值
        dataVO.setCode(0);
        dataVO.setMsg("");

        dataVO.setCount(userService.queryUserNums());
//        PageHelper.startPage(page, limit);

        List<UserVO> userVOS = userService.queryAllUserVO(userName);
//        PageInfo<UserVO> pageInfo = new PageInfo<>(userVOS);

        dataVO.setData(userVOS);

        return dataVO;
    }

    /*跳转修改用户页面*/
    @RequestMapping("/page/userUpdateForm")
    public String userForm() {
        return "user_update_Form";
    }

    /**
     * 正式修改
     *
     * @param userId
     * @param newUserName
     * @param newUserPassword
     * @param newUserEmail
     * @param newUserAge
     * @return
     */
    @RequestMapping("/page/updateUser")
    @ResponseBody
    public String updUser(Long userId, String newUserName,
                          String newUserPassword, String newUserEmail,
                          Integer newUserAge) {


        User user = new User(userId, newUserName, newUserPassword, newUserEmail, newUserAge);

        int state = userService.updateUserInfo(user); //调用更新user对象的：业务层代码

        if (state > 0) {      //状态 > 0 时： 表示更新成功，否则失败
            return "true";
        }
        return "false";
    }

    /**
     * 跳转用户添加页面
     *
     * @return
     */
    @RequestMapping("/page/userAddPage")
    public String userAddPage() {
        return "user_add";
    }

    /**
     * 正式添加用户界面
     *
     * @param user
     * @param model
     * @return
     */
    @RequestMapping("/page/insertUser")
    public String userAdd(User user, Model model) {
        int state = userService.insUserByUser(user);    //添加用户

        if (state > 0) {
            model.addAttribute("userAddMessage", "true"); //用户添加成功
        } else {
            model.addAttribute("userAddMessage", "false");//用户添加失败
        }

        System.out.println(state);

        return "user_add";
    }


    /**
     * 删除用户
     *
     * @param userId
     * @return
     */
    @RequestMapping("/page/deleteUser")
    @ResponseBody
    public String deleteUser(Long userId) {
        int state = userService.delUserByUserId(userId);

        if (state > 0)
            return "true";

        return "false";
    }

    /**
     * 删除多个用户
     *
     * @param ids
     * @return
     */
    @RequestMapping("/page/deleteAllUser")
    @ResponseBody
    public String deleteAllUser(Long[] ids) {
        for (Long id : ids) {
            userService.delUserByUserId(id);
        }
        return "true";
    }


    /**
     * 跳转到个人信息
     *
     * @param session
     * @param model
     * @return
     */
    @GetMapping("/page/info")
    public String userInfo(HttpSession session, Model model) {
        User currentUser = (User) session.getAttribute("user");
        User user = userService.queryUserById(currentUser.getUserId());
        model.addAttribute("user", user);
        return "user_info";
    }

    /**
     * 更新个人信息
     *
     * @return
     */
    @GetMapping("/page/updateInfo")
    public String updateInfo() {
        return "user_edit";
    }

    /**
     * 正式更新个人信息
     *
     * @param userId
     * @param userName
     * @param userPassword
     * @param userEmail
     * @param userAge
     * @return
     */
    @PostMapping("/page/updateForm")
    public String updateForm(Long userId, String userName,
                             String userPassword, String userEmail,
                             Integer userAge) {
        User user = new User(userId, userName, userPassword, userEmail, userAge);
        logger.info("user ===>>> " + user);

        userService.updateUserInfo(user); //调用更新user对象的：业务层代码

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return "redirect:/user/page/updateInfo";
    }


}
