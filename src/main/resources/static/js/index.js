//柱状图1
(function () {

    var mychart = echarts.init(document.querySelector('.bar .chart'));

    var option = {
        color: ["#2f89cf"],
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow'
          }
        },
        grid: {
          left: '0%',
          right: '0%',
          top: "10px",
          bottom: '1%',
          containLabel: true
        },
        xAxis: [
          {
            type: 'category',
            data: ['旅游行业', '教育培训', '游戏行业', '医疗行业', '电商行业', '社交行业', '金融行业'],
            axisTick: {
              alignWithLabel: true
            },
            axisLabel: {
              color: "rgba(255,255,255,0.6)",
              fontSize: "12px"
            },
            axisLine: {
              show: false
            }
          }
        ],
        yAxis: [
          {
            type: 'value',
            axisLabel: {//
              color: "rgba(255,255,255,0.6)",
              fontSize: 5
            },
            axisLine: {//y轴轴线
              lineStyle: {
                color : "rgba(255,255,255,.1)",
                width:2
              }
            },
            splitLine: {//分割线颜色
              lineStyle: {
                color: "rgba(255,255,255,0.1)"
              }
            }
          }
        ],
        series: [
          {
            name: 'Direct',
            type: 'bar',
            barWidth: '35%',//宽度
            data: [200, 300, 200, 800, 1500, 1200, 600],
            itemStyle: {
              barBorderRadius:5//圆角
            }
          }
        ]
      };

      mychart.setOption(option);
      window.addEventListener('resize',function(){
        mychart.resize();
      });
})();

//柱状图2
(function () {

  var mychart = echarts.init(document.querySelector('.bar2 .chart'));

  var myColor = ["#1089e7","#f57474","#56D0e3","f8b448","#8b78f6","blue"];
  var  option = {

    grid: {
      left: '22%',
      top: "10%",
      bottom: '10%',

    },
    xAxis: {
      show: false,
      type: 'value',
      boundaryGap: [0, 0.01]
    },
    yAxis: [
      {

        type: 'category',
        inverse: true,
        data: ['计算机科学', '会计教育', '软件工程', '工商管理', '旅游管理', '金融管理'],
        axisLine: {//不显示y轴
          show: false
        },
        axisTick: {//不显示刻度
          show: false
        },
        axisLabel: {
          color: "#fff"
        }
      },
      {
        // show: false,
        data:  [702,350,611,790,666,400],
        inverse: true,
        type: 'category',
        
        axisLine: {//不显示y轴
          show: false
        },
        axisTick: {//不显示刻度
          show: false
        },
        axisLabel: {
          color: "#fff"
        }
      }
    ],
    series: [
      {
        name: '条',
        barCategoryGap: 50,//柱子之间的距离
        barWidth: 10,//柱子的宽度
        yAxisIndex: 0,
        itemStyle: {
          normal: {
            barBorderRadius: 20,//圆角
            color: function(params){//给itemStyle 里面的color属性设置一个返回值函数
              var num = myColor.length;
              return myColor[params.dataIndex % num];
            }
          }
        },
        type: 'bar',
        data: [70, 34, 60, 78, 69, 30],
        label: {
          normal: {
            show: true,
            //图形内显示
            position: "inside",
            //文字的显示格式
            formatter: "{c}%"
          }
        }
      },
      {
        name: '框',
        type: 'bar',
        yAxisIndex: 1,
        barCategoryGap: 50,
        barWidth: 15,
        itemStyle: {
          color: "none",
          borderColor: "#00c1de",
          borderWidth: 3,
          barBorderRadius: 15  
        },
        data: [100, 100, 100, 100, 100, 100]
      }
    ]
  };

    mychart.setOption(option);
    window.addEventListener('resize',function(){
      mychart.resize();
    });
})();

//折线图
(function () {
  var mychart = echarts.init(document.querySelector('.line .chart'));

  option = {
    title: {
      text: '就业对比',
      textStyle: {
        color: "rgba(255,255,255,.6)"
      }
    },
    legend: {
      data: ['2020年','2021年'],
      textStyle: {
        color: "#FFF"
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    xAxis: {
      type: 'category',
      data: ['1月', '','3月','', '5月','', '7月','', '9月', '','11月', ''],
      axisLabel: {
        color: "rgba(255,255,255,.6)"
      }
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        color: "rgba(255,255,255,.6)"
      }
    },
    series: [
      {
        name: '2020年',
        data: [50, 100, 290, 450, 360, 560, 500,420,300,400,380,200],
        type: 'line',
        smooth: true
      },
      {
        name: '2021年',
        data: [30, 45, 90, 120, 100, 215, 200,210,110,210,200,115],
        type: 'line',
        smooth: true
      }
    ]
  };
  // var legend = {
  //   data:legendData
  // }
  // option.legend = legend;
    mychart.setOption(option);
    window.addEventListener('resize',function(){
      mychart.resize();
    });
})();

//折线图2
(function () {

  var mychart = echarts.init(document.querySelector('.line2 .chart'));

  option = {
    
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    legend: {
      
      data: ['10k以上', '10k以下'],
      textStyle: {
        color: "rgba(255,255,255,.6)",
        fontSize: 12
      }
    },
   
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        boundaryGap: false,
        data: ['华为', '腾讯', '网易', '字节跳动', '百度', '谷歌', '农夫山泉'],
        axisLabel: {
          color: "rgba(255,255,255,.6)"
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        axisLabel: {
          color: "rgba(255,255,255,.6)"
        }
      }
    ],
    series: [
      {
        name: '10k以上',
        type: 'line',
        stack: 'Total',
        areaStyle: {},
        emphasis: {
          focus: 'series'
        },
        data: [120, 132, 101, 134, 90, 230, 210],
        smooth: true,
      
        symbol: "circle",
        symbolSize: 5,
        itemStyle: {
          borderColor: "rgba(221,220,107,.5)",
          borderWidth: 12
        },
        showSymbol: false//拐点
        
      },
      {
        name: '10k以下',
        type: 'line',
        stack: 'Total',
        areaStyle: {},
        emphasis: {
          focus: 'series'
        },
        data: [220, 182, 191, 234, 290, 330, 310],
        smooth: true,
        symbol: "circle",
        symbolSize: 5,
        itemStyle: {
          borderColor: "rgba(221,220,107,.5)",
          borderWidth: 12
        },
        showSymbol: false//拐点
        
      }
    ]
  };
    mychart.setOption(option);
    window.addEventListener('resize',function(){
      mychart.resize();
    });
})();

//饼状图
(function () {

  var mychart = echarts.init(document.querySelector('.pie .chart'));

  option = {
    tooltip: {
      trigger: 'item'
    },
    legend: {
      orient: "vertical",
      top: '5%',
      left: 10,
      textStyle: {
        color: "rgba(255,255,255,.6)"
      }
    },
    series: [
      {
        name: '就业年龄分布',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        label: {
          show: false,
          position: 'center'
        },
        
        labelLine: {
          show: false
        },
        data: [
          { value: 1048, name: '31-40岁' },
          { value: 735, name: '21-30岁' },
          { value: 580, name: '41-50岁' },
          { value: 484, name: '50岁以上' },
          { value: 300, name: '20岁以下' }
        ]
      }
    ]
  };
    mychart.setOption(option);
    window.addEventListener('resize',function(){
      mychart.resize();
    });
})();

//饼状图2
(function () {

  var mychart = echarts.init(document.querySelector('.pie2 .chart'));

  option = {
  
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    
    series: [
      {
        name: 'Area Mode',
        type: 'pie',
        radius: [20, 130],
        center: ['50%', '55%'],
        roseType: 'area',
        itemStyle: {
          borderRadius: 5,
          
        },
        data: [
          { value: 30, name: '云南' },
          { value: 28, name: '北京' },
          { value: 26, name: '南宁' },
          { value: 24, name: '深圳' },
          { value: 22, name: '桂林' },
          { value: 20, name: '山东' },
          { value: 18, name: '浙江' },
          { value: 16, name: '四川' }
        ],
        label: {
          color: "#fff"
        }
      }
    ]
  };
    mychart.setOption(option);
    window.addEventListener('resize',function(){
      mychart.resize();
    });
})();
