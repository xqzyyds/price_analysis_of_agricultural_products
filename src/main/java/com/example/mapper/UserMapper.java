package com.example.mapper;

import com.example.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.Mapping;

import java.util.List;

/**
 * @author xqz
 * @description TODO
 * @dateTime 2023/5/30 9:31
 */
@Mapper
public interface UserMapper {

    /**
     * 通过用户名获取用户信息
     * @author xqz
     * @apiNote 通过用户名获取用户信息
     * @dateTime 2023/5/30 9:38
     * @param username 用户名
     * @return User 用户信息
     */
    //1、通过用户名获取用户信息
    User getUserInfoByUserName(@Param("username") String username);

    //2、查询：所有用户（返回List）
    List<User> getAllUsers(String userName);

    //3、查询：用户的数目
    Long selUserCount();

    //4、查询：通过userId
    User selUserByUserId(Long userId);

    //5、更新：通过user (更新用户信息)
    int updUserByUser(User user);

    //6、添加用户：通过user对象进行添加
    int insertUserByUser(User user);

    //7、删除用户：通过userId
    int deleteUserByUserId(Long Userid);

    /*8、查询：通过名称查询User对象*/
    User getUserByUsername(String username);

    /*9、添加用户：博客前台添加用户*/
    void save(User user);
}
