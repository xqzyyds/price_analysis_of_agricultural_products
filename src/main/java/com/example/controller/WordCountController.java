package com.example.controller;

import com.example.entity.PriceData;
import com.example.service.impl.HBaseServicePlus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

import static com.example.util.TimeConstant.MONTHS_COUNT;

/**
 * @author xqz
 * @description TODO
 * @dateTime 2023/5/8 12:10
 */
@Controller
public class WordCountController {

    @Resource
    private HBaseServicePlus hBaseServicePlus;

    /**
     * 获取某个地区某年的所有月份数据
     * @author xqz
     * @apiNote 获取某个地区某年的所有月份数据
     * @dateTime 2023/5/20 20:01
     */
    @GetMapping("/test")
    @ResponseBody
    public ResponseEntity<List<PriceData>> test(@RequestParam String province,
                                                @RequestParam String year) {
        List<PriceData> dataList = new ArrayList<>();
        try {
            for (int i = 1; i <= MONTHS_COUNT; i++) {
                PriceData data = hBaseServicePlus.getHBaseData(province, year, String.format("%02d", i));
                dataList.add(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(dataList, HttpStatus.OK);
    }


    /**
     * 展示1: 获取某省份所有年份所有月份的数据
     * @author xqz
     * @apiNote 获取某省份所有年份所有月份的数据
     * @dateTime 2023/5/17 20:56
     * @param province 省份
     */
    @GetMapping("/data/province/years/months")
    @ResponseBody
    public ResponseEntity<Map<String, List<PriceData>>> test2(@RequestParam String province) {
        // 定义年份
        List<String> years = Arrays.asList("2018", "2019", "2020", "2021");
        // 初始化一个集合, 用来装查询到的数据
        Map<String, List<PriceData>> dataMap = new HashMap<>(48);
        try {
            for (String year : years) {
                List<PriceData> dataList = new ArrayList<>();
                for (int i = 1; i <= MONTHS_COUNT; i++) {
                    PriceData data = hBaseServicePlus.getHBaseData(province, year, String.format("%02d", i));
                    dataList.add(data);
                }
                dataMap.put(year, dataList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(dataMap, HttpStatus.OK);
    }

    @GetMapping("/bigdisplay/search")
    public String bigDisplaySearchPage() {
        return "big_display_search";
    }

    /**
     * 获取某一年所有省份的所有平均价格
     * @author xqz
     * @apiNote 获取某一年所有省份的所有平均价格
     * @dateTime 2023/5/21 9:45
     * @param year 某年(2018-2021)
     */
    @GetMapping("/bigdisplay/index")
    public String index(@RequestParam String year, @RequestParam String province, Model model) {
        List<PriceData> priceDataList;
        try {
            priceDataList = hBaseServicePlus.getAllYearOfProvinceData(year);
            // 获取最大值和最小值,第一个元素为最小值 第二个元素为最大值
            PriceData minValueData = priceDataList.get(0);
            PriceData maxValueData = priceDataList.get(1);

            // 从列表中删除最大值和最小值对象
            priceDataList.remove(minValueData);
            priceDataList.remove(maxValueData);

            model.addAttribute("priceDataList", priceDataList);
            model.addAttribute("minValue", minValueData.getAveragePrice());
            model.addAttribute("maxValue", maxValueData.getAveragePrice());
            model.addAttribute("year", year);
            model.addAttribute("province", province);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Demo";
    }

    /**
     * 获取某年 所有月份， 所有省份的价格
     * @author xqz
     * @apiNote 获取某年 所有月份 所有省份 的价格
     * @dateTime 2023/5/22 22:01
     * @param year 年份
     * @return ResponseEntity<Map<String,Map<String,Double>>> 返回一个月份：省份：价格的集合
     */
    @GetMapping("/dynamic_bar_chart")
    public ResponseEntity<Map<String, Map<String, Double>>> dynamicBarChart(@RequestParam String year) {
        Map<String, Map<String, Double>> data = new HashMap<>(12);
        try {
            data = hBaseServicePlus.getAllProvinceMonthDataByYear(year);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 获取某个省份某年某月的该月内的所有天数的平均价格
     * @author xqz
     * @apiNote 获取某个省份某年某月的该月内的所有天数的平均价格
     * @dateTime 2023/5/29 15:57
     * @param province 省份
     * @param year 年份
     * @param month 月份
     * @return <List<PriceData>, 返回集合
     */
    @GetMapping("/data/province/year/month/days")
    public ResponseEntity<List<PriceData>> getDayDataByProvinceAndYearAndMonth(@RequestParam String province,
                                                                               @RequestParam String year,
                                                                               @RequestParam Integer month) {
        try {
            List<PriceData> data = hBaseServicePlus.getHBaseDataForMonth(province, year, String.format("%02d", month));
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}