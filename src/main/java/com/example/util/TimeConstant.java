package com.example.util;

/**
 * @author xqz
 * @description TODO
 * @dateTime 2023/5/17 11:04
 */
public class TimeConstant {
    public static final int MAX_SIZE = 100;
    public static final int TIMEOUT = 5000;
    public static final String DEFAULT_NAME = "John Doe";
    public static final String DEFAULT_MONTH = "01";
    public static final int MONTHS_COUNT = 12;
}
