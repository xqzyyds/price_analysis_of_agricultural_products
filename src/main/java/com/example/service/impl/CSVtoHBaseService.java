package com.example.service.impl;

import com.example.service.impl.HBaseServicePlus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Service
public class CSVtoHBaseService {

    @Autowired
    private HBaseServicePlus hBaseService;

    /**
     * 插入某地区某年某月份平均价格
     * @author xqz
     * @apiNote 读取CSV文件,将某地区某年份某月份的平均价格数据插入到Hbase中
     * @dateTime 2023/5/17 11:34
     * @param filePath 文件路径
     */
    public void loadCSVtoHBase(String filePath) {
        try {
            ClassPathResource classPathResource = new ClassPathResource(filePath);
            InputStreamReader inputStreamReader = new InputStreamReader(classPathResource.getInputStream());
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line;
            boolean firstLine = true;
            while((line = reader.readLine()) != null) {
                if (firstLine) {
                    // 如果是第一行（头部行），跳过
                    firstLine = false;
                    continue;
                }

                // 数据是逗号分隔的
                String[] data = line.split(",");

                // 确保该行具有正确数量的字段
                if(data.length == 4) {
                    String province = data[0];
                    String year = data[1];
                    // 如果月份是一位数，将其改为两位数
                    String month = String.format("%02d", Integer.parseInt(data[2]));
                    String averagePrice = data[3];

                    // 将数据写入HBase
                    hBaseService.putHBaseData(province, year, month, averagePrice);
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 插入某地区某年平均价格
     * @author xqz
     * @apiNote 读取CSV文件,将某地区某年份的平均价格数据插入到Hbase中
     * @dateTime 2023/5/17 11:34
     * @param filePath 文件路径
     */
    public void loadYearCSVtoHBase(String filePath) {
        try {
            ClassPathResource classPathResource = new ClassPathResource(filePath);
            InputStreamReader inputStreamReader = new InputStreamReader(classPathResource.getInputStream());
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line;
            boolean firstLine = true;
            while((line = reader.readLine()) != null) {
                if (firstLine) {
                    // 如果是第一行（头部行），跳过
                    firstLine = false;
                    continue;
                }

                // 数据是逗号分隔的
                String[] data = line.split(",");

                // 确保该行具有正确数量的字段
                if(data.length == 3) {
                    String province = data[0];
                    String year = data[1];
                    String averagePrice = data[2];

                    // 将数据写入HBase
                    hBaseService.putHBaseData(province, year, averagePrice);
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadCSVtoHBase2(String filePath) {
        try {
            ClassPathResource classPathResource = new ClassPathResource(filePath);
            InputStreamReader inputStreamReader = new InputStreamReader(classPathResource.getInputStream());
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line;
            boolean firstLine = true;
            while((line = reader.readLine()) != null) {
                if (firstLine) {
                    // 如果是第一行（头部行），跳过
                    firstLine = false;
                    continue;
                }

                // 数据是逗号分隔的
                String[] data = line.split(",");

                // 确保该行具有正确数量的字段
                if(data.length == 4) {
                    String province = data[0];
                    String year = data[1];
                    // 如果月份是一位数，将其改为两位数
                    String month = String.format("%02d", Integer.parseInt(data[2]));
                    String averagePrice = data[3];

                    // 将数据写入HBase
                    hBaseService.putDataByDynamicBarChart(province, year, month, averagePrice);
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
