package com.example;

import com.example.entity.PriceData;
import com.example.service.AreaService;
import com.example.service.impl.CSVtoHBaseService;
import com.example.service.impl.HBaseServicePlus;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@SpringBootTest(classes = PriceAnalysisOfAgriculturalProductsApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
class PriceAnalysisTests {

    @Autowired
    private HBaseServicePlus hBaseServicePlus;

    @Autowired
    private CSVtoHBaseService csvToHBaseService;

    @Autowired
    private AreaService areaService;

    // 插入某地区某年某月数据
    @Test
    public void testLoadCSVtoHBase() {
        String filePath = "static/year_and_month.csv"; // replace with your CSV file path
        csvToHBaseService.loadCSVtoHBase(filePath);
    }

    // 插入某地区某年某月数据,动态柱形图
    @Test
    public void testLoadCSVtoHBase2() {
        String filePath = "static/year_and_month.csv"; // replace with your CSV file path
        csvToHBaseService.loadCSVtoHBase2(filePath);
    }

    // 插入某地区某年数据
    @Test
    public void testLoadYearCSVtoHBase() {
        String filePath = "static/result_year.csv"; // replace with your CSV file path
        csvToHBaseService.loadYearCSVtoHBase(filePath);
    }

    // 测试获取某地区某年某月数据
    @Test
    public void testGetHBaseData() {
        try {
            PriceData data = hBaseServicePlus.getHBaseData("上海", "2018", "01");
            System.out.println(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // 测试获取某地区某年某月数据, 动态柱形图
    @Test
    public void testGetHBaseData2() {
        try {
            Map<String, Map<String, Double>> data = hBaseServicePlus.getAllProvinceMonthDataByYear("2018");
            System.out.println(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // 测试获取某地区某年数据
    @Test
    public void testGetYearHBaseData() {
        try {
            PriceData data = hBaseServicePlus.getHBaseData("上海", "2019");
            System.out.println(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetDayHBaseData() throws IOException {
        List<PriceData> data = hBaseServicePlus.getHBaseDataForMonth("北京", "2018", "02");
        System.out.println(data);
    }

    @Test
    public void testDataTotal() throws IOException {
        int count = areaService.getOverallCount();
        System.out.println("count = " + count);
    }

    @Test
    public void testDataProvinceTotal() throws IOException {
        int count = areaService.getProvinceCount();
        System.out.println("count = " + count);
    }

    @Test
    public void testDataMarketTotal() throws IOException {
        int count = areaService.getMarketCount();
        System.out.println("count = " + count);
    }
}
