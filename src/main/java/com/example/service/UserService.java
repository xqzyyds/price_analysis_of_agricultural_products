package com.example.service;

import com.example.entity.User;
import com.example.vo.UserVO;

import java.util.List;

/**
 * @author xqz
 * @description TODO
 * @dateTime 2023/5/30 9:23
 */
public interface UserService {
    User checkUser(String username, String password);

    List<UserVO> queryAllUserVO(String userName);

    Long queryUserNums();

    int updateUserInfo(User user);

    //5、添加用户：通过user对象进行添加
    int insUserByUser(User user);

    //6、删除用户：通过userId
    int delUserByUserId(Long userId);

    User queryUserById(Long id);
}
