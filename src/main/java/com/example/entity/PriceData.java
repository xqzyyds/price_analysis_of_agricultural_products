package com.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PriceData {
    private String province;
    private String year;
    private String month;
    private Double averagePrice;
    private String day;
    private Double maxPrice;

    // 构造函数
    public PriceData(String province, String year, String month, Double averagePrice) {
        this.province = province;
        this.year = year;
        this.month = month;
        this.averagePrice = averagePrice;
    }

    public PriceData(String province, String year, String month, Double averagePrice, String day) {
        this.province = province;
        this.year = year;
        this.month = month;
        this.averagePrice = averagePrice;
        this.day = day;
    }

    public PriceData(String province, String year, String month, Double averagePrice, Double maxPrice, String day) {
        this.province = province;
        this.year = year;
        this.month = month;
        this.averagePrice = averagePrice;
        this.maxPrice = maxPrice;
        this.day = day;
    }
    // Getter 和 Setter 方法

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Double getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(double averagePrice) {
        this.averagePrice = averagePrice;
    }
}
