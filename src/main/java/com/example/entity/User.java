package com.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Long userId;
    private String userName;
    private String userPassword;
    private String userEmail;
    private Integer type;
    private Integer userAge;
    private Date createTime;

    public User(Long userId, String userName, String userPassword, String userEmail, Integer userAge) {
        this.userId = userId;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.userAge = userAge;
    }
}
