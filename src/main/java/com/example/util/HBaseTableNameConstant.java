package com.example.util;

/**
 * @author xqz
 * @description TODO
 * @dateTime 2023/5/28 21:35
 */
public class HBaseTableNameConstant {
    public static final String AVG_PRICE_DATA_OF_PROVINCES_AND_MONTHS_BY_YEAR_TABLE = "avg_price_data_of_provinces_and_months_by_year_table";
    public static final String AVG_PRICE_DATA_OF_PROVINCES_AND_MONTHS_BY_YEAR_TABLE_COLUMN_NAME = "info";

    public static final String AVG_PRICE_DATA_OF_MONTHS_BY_YEAR_AND_PROVINCE_TABLE = "avg_price_data_of_months_by_year_and_province_table";
    public static final String AVG_PRICE_DATA_OF_MONTHS_BY_YEAR_AND_PROVINCE_TABLE_COLUMN_NAME = "info";

    public static final String AVG_PRICE_DATA_OF_PROVINCES_BY_YEAR_TABLE = "avg_price_data_of_provinces_by_year_table";
    public static final String AVG_PRICE_DATA_OF_PROVINCES_BY_YEAR_TABLE_COLUMN_NAME = "province";

    public static final String OVERALL_STATS_TABLE_NAME = "overall_stats_table";
    public static final String STATS_COLUMN_FAMILY = "stats";
    public static final String COUNT_COLUMN = "count";

    public static final String PROVINCE_STATS_TABLE_NAME = "province_stats_table";

    public static final String MARKET_STATS_TABLE_NAME = "market_stats_table";
}
